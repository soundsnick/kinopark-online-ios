//
//  WebView.swift
//  kinopark-online
//
//  Created by yernazar on 20.04.2022.
//

import Foundation
import SwiftUI
import Combine
import WebKit
import UIKit

struct WebView: UIViewRepresentable {
    
    var type: URLType
    var url: String?
    
    func makeUIView(context: Context) -> WKWebView {
        let preferences = WKPreferences()
        
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        configuration.websiteDataStore = WKWebsiteDataStore.default()
        
        let webView = WKWebView(frame: CGRect.zero, configuration: configuration)
        
        webView.allowsBackForwardNavigationGestures = true
        webView.scrollView.isScrollEnabled = true

        
//        webView.configuration.userContentController.addUserScript(script())
//
//        func script() -> WKUserScript {
//
//         let source = "localStorage.setItem('token', 'WwrI6HYLJuRMyIn77eK3bkDvOTD5bY');"
//
//         return WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
//        }
        
        return webView
    
    }
    
    func updateUIView(_ webView: WKWebView, context: Context) {
        if let urlValue = url  {
            if let requestUrl = URL(string: urlValue) {
                webView.load(URLRequest(url: requestUrl))
            }
        }
    }
}
