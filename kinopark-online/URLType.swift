//
//  URLType.swift
//  kinopark-online
//
//  Created by yernazar on 20.04.2022.
//

import Foundation
enum URLType {
    case local, `public`
}
