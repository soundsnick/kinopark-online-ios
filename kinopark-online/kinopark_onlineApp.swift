//
//  kinopark_onlineApp.swift
//  kinopark-online
//
//  Created by yernazar on 20.04.2022.
//

import SwiftUI

@main
struct kinopark_onlineApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
