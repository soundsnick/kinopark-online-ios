//
//  ContentView.swift
//  kinopark-online
//
//  Created by yernazar on 20.04.2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        WebView(type: .public, url: "http://localhost:3001")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
